Copy of (modified) east-adl git in bitbucket git

Meta-model creation (ecore/xsd/genmodel from .EAP file):
  Before creating a specific version (in a plugin called org.eclipse.eatop.eastadlxx), adapt settings in
  class IEASTADLGeneratorConstants. The class is in the plugin
  metamodelgen/org.eclipse.eatop.metamodelgen.serialization.generators
  (package org.eclipse.eatop.metamodelgen.serialization.generators.util)

  CAVEAT: if MM generation is invoked with the wrong version number, carefully
  	delete old artefacts (e.g. reference to lib/xxx, clean source folder)

Compile tooling:
  invoke mvn in releng/org.eclipse.eatop.releng.builds
  See readme.txt file there for more details
