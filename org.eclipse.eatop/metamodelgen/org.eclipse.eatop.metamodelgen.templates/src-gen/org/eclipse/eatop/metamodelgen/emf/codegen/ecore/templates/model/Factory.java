package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.model;

import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.common.util.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.*;

public class Factory
{
  protected static String nl;
  public static synchronized Factory create(String lineSeparator)
  {
    nl = lineSeparator;
    Factory result = new Factory();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = "/**";
  protected final String TEXT_3 = NL + " * ";
  protected final String TEXT_4 = NL + " * <copyright>" + NL + " * " + NL + " * Copyright (c) 2014 itemis and others." + NL + " * All rights reserved. This program and the accompanying materials" + NL + " * are made available under the terms of the Eclipse Public License v1.0" + NL + " * which accompanies this distribution, and is available at" + NL + " * http://www.eclipse.org/legal/epl-v10.html" + NL + " * " + NL + " * Contributors: " + NL + " *     itemis - Initial API and implementation" + NL + " * " + NL + " * </copyright>";
  protected final String TEXT_5 = NL + " */" + NL + "package ";
  protected final String TEXT_6 = ";" + NL;
  protected final String TEXT_7 = NL + NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * The <b>Factory</b> for the model." + NL + " * It provides a create method for each non-abstract class of the model." + NL + " * <!-- end-user-doc -->" + NL + " */" + NL + "public class ";
  protected final String TEXT_8 = " extends ";
  protected final String TEXT_9 = " implements ";
  protected final String TEXT_10 = " {" + NL + "" + NL + "    public static final ";
  protected final String TEXT_11 = " eINSTANCE = new ";
  protected final String TEXT_12 = "();" + NL + "" + NL + "    /**" + NL + "    * Returns the value of the '<em><b>EPackage</b></em>' reference." + NL + "    */" + NL + "    @Override" + NL + "    public ";
  protected final String TEXT_13 = " getEPackage()" + NL + "    {" + NL + "        if (ePackage == null)" + NL + "        {" + NL + "            ePackage = ";
  protected final String TEXT_14 = ".eINSTANCE;" + NL + "        }" + NL + "        return ePackage;" + NL + "    }" + NL + "\t" + NL + "    /**" + NL + "    * <!-- begin-user-doc -->" + NL + "    * Creates a new instance of the class and returns it." + NL + "    * Ask the package the list of its classifiers. Given these classifiers, ask their packages what their factories are." + NL + "    * @param eClass the class of the new instance." + NL + "    * @return a new instance of the class." + NL + "    * <!-- end-user-doc -->" + NL + "    */" + NL + "    @Override" + NL + "    public ";
  protected final String TEXT_15 = " create(";
  protected final String TEXT_16 = " eClass)" + NL + "    {" + NL + "        EObject result = null;" + NL + "" + NL + "        if (eClass.eContainer() instanceof EPackage)" + NL + "        {" + NL + "            result = ((EPackage) eClass.eContainer()).getEFactoryInstance().create(eClass);" + NL + "        }" + NL + "        return result;" + NL + "    }" + NL + "\t";
  protected final String TEXT_17 = NL + "    public ";
  protected final String TEXT_18 = " create";
  protected final String TEXT_19 = "() {";
  protected final String TEXT_20 = NL + "        ";
  protected final String TEXT_21 = " ";
  protected final String TEXT_22 = " = ";
  protected final String TEXT_23 = ".eINSTANCE.create";
  protected final String TEXT_24 = "();";
  protected final String TEXT_25 = ".setUuid(java.util.UUID.randomUUID().toString());" + NL + "        return ";
  protected final String TEXT_26 = ";";
  protected final String TEXT_27 = NL + "        return ";
  protected final String TEXT_28 = NL + "    }" + NL + "    ";
  protected final String TEXT_29 = NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

    GenPackage genPackage = (GenPackage)argument; GenModel genModel=genPackage.getGenModel();
    EClass identifiableClass = null;
    TreeIterator<EObject> iterator = genModel.eAllContents();
    while (iterator.hasNext()) {
    EObject currentObject = iterator.next();
    if (currentObject instanceof GenClass && "Identifiable".equals(((GenClass) currentObject).getEcoreClass().getName())) {
    identifiableClass = ((GenClass)currentObject).getEcoreClass();
    break;
    }
    }
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_4);
    }}
    stringBuffer.append(TEXT_5);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getUtilitiesPackageName());
    stringBuffer.append(TEXT_6);
    genModel.markImportLocation(stringBuffer);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "Factory"));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.EFactoryImpl"));
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EFactory"));
    stringBuffer.append(TEXT_10);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "Factory"));
    stringBuffer.append(TEXT_11);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "Factory"));
    stringBuffer.append(TEXT_12);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EPackage"));
    stringBuffer.append(TEXT_13);
    stringBuffer.append(genModel.getImportedName("org.eclipse.eatop.eastadl22.Eastadl22Package"));
    stringBuffer.append(TEXT_14);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_15);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_16);
    iterator = genModel.eAllContents();
    while (iterator.hasNext()) {
    EObject currentObject = iterator.next();
    if (currentObject instanceof GenClass) {
    EClass currentClass = ((GenClass)currentObject).getEcoreClass();
    if (!currentClass.isAbstract()) {
    String qualifiedPckName = EClasses.getQualifiedPackageName(currentClass);
    String basePackage = "org.eclipse.eatop.";
    String factoryName = basePackage + EClasses.getFactoryQName(currentClass, qualifiedPckName);
    String eastadlPackageComplete = basePackage + IEASTADLTemplateConstants.EASTADL_ROOT_PACKAGE  + ".";
    String currentClassName =currentClass.getName();
    stringBuffer.append(TEXT_17);
    stringBuffer.append(eastadlPackageComplete);
    stringBuffer.append(currentClassName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(currentClassName);
    stringBuffer.append(TEXT_19);
    if (currentClass.getEAllSuperTypes().contains(identifiableClass)) {
    String variableName = currentClassName.substring(0, 1).toLowerCase() + currentClassName.substring(1);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(eastadlPackageComplete);
    stringBuffer.append(currentClassName);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(variableName);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(factoryName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(currentClassName);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(variableName);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(variableName);
    stringBuffer.append(TEXT_26);
    } else {
    stringBuffer.append(TEXT_27);
    stringBuffer.append(factoryName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(currentClassName);
    stringBuffer.append(TEXT_24);
    }
    stringBuffer.append(TEXT_28);
    }
    }
    }
    stringBuffer.append(TEXT_29);
    genModel.emitSortedImports();
    return stringBuffer.toString();
  }
}
