package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.edit;

import org.eclipse.emf.codegen.ecore.genmodel.*;
import java.util.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;
import org.eclipse.eatop.metamodelgen.templates.internal.util.EASTADLGenModels;

public class BuildProperties
{
  protected static String nl;
  public static synchronized BuildProperties create(String lineSeparator)
  {
    nl = lineSeparator;
    BuildProperties result = new BuildProperties();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "# ";
  protected final String TEXT_2 = NL + "#  <copyright>" + NL + "#" + NL + "# Copyright (c) 2014 itemis and others." + NL + "# All rights reserved. This program and the accompanying materials" + NL + "# are made available under the terms of the Eclipse Public License v1.0" + NL + "# which accompanies this distribution, and is available at" + NL + "# http://www.eclipse.org/legal/epl-v10.html" + NL + "# " + NL + "# Contributors: " + NL + "#     itemis - Initial API and implementation" + NL + "# " + NL + "# </copyright>";
  protected final String TEXT_3 = NL + NL + "bin.includes = ";
  protected final String TEXT_4 = ",\\";
  protected final String TEXT_5 = NL + "               icons/,\\";
  protected final String TEXT_6 = NL + "               META-INF/,\\";
  protected final String TEXT_7 = NL + "               plugin.xml,\\";
  protected final String TEXT_8 = NL + "               plugin.properties";
  protected final String TEXT_9 = ",\\" + NL + "               lib/";
  protected final String TEXT_10 = ".edit.jar";
  protected final String TEXT_11 = NL + "jars.compile.order = ";
  protected final String TEXT_12 = NL + "src.includes = lib/";
  protected final String TEXT_13 = ".editsrc.zip";
  protected final String TEXT_14 = NL + "source.";
  protected final String TEXT_15 = " = ";
  protected final String TEXT_16 = NL + "output.";
  protected final String TEXT_17 = " = bin/";
  protected final String TEXT_18 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * Copyright (c) 2002-2010 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: 
 *   IBM - Initial API and implementation
 */

    GenModel genModel = (GenModel)argument;
    String pluginClassesLocation = genModel.isRuntimeJar() ? genModel.getEditPluginID()+".jar" : ".";
    List<String> sourceFolders = genModel.getEditSourceFolders();
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_1);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_2);
    }}
    stringBuffer.append(TEXT_3);
    stringBuffer.append(pluginClassesLocation);
    stringBuffer.append(TEXT_4);
    if (genModel.getRuntimePlatform() != GenRuntimePlatform.GWT) {
    stringBuffer.append(TEXT_5);
    }
    if (genModel.isBundleManifest()) {
    stringBuffer.append(TEXT_6);
    }
    if (genModel.getRuntimePlatform() != GenRuntimePlatform.GWT) {
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_8);
    if (EASTADLGenModels.IsLibJarCreated(genModel)) {
    stringBuffer.append(TEXT_9);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getEcorePackage().getName());
    stringBuffer.append(TEXT_10);
    }
    stringBuffer.append(TEXT_11);
    stringBuffer.append(pluginClassesLocation);
    if (EASTADLGenModels.IsLibSrcZipCreated(genModel)) {
    stringBuffer.append(TEXT_12);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getEcorePackage().getName());
    stringBuffer.append(TEXT_13);
    }
else{
     boolean first=true; for (Iterator<String> i = sourceFolders.iterator(); i.hasNext();) { String sourceFolder = i.next(); if (i.hasNext()){sourceFolder +=",\\";} if (first) {
    stringBuffer.append(TEXT_14);
    stringBuffer.append(pluginClassesLocation);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(sourceFolder);
    first=false;} else {
    stringBuffer.append(sourceFolder);
    }}
    stringBuffer.append(TEXT_16);
    stringBuffer.append(pluginClassesLocation);
    stringBuffer.append(TEXT_17);
    }

    stringBuffer.append(TEXT_18);
    return stringBuffer.toString();
  }
}
