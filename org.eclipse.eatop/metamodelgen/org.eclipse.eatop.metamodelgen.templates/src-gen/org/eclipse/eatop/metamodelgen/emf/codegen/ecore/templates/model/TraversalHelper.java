package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.model;

import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.*;

public class TraversalHelper
{
  protected static String nl;
  public static synchronized TraversalHelper create(String lineSeparator)
  {
    nl = lineSeparator;
    TraversalHelper result = new TraversalHelper();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = "/**";
  protected final String TEXT_3 = NL + " * ";
  protected final String TEXT_4 = NL + " * <copyright>" + NL + " * " + NL + " * Copyright (c) 2014 itemis and others." + NL + " * All rights reserved. This program and the accompanying materials" + NL + " * are made available under the terms of the Eclipse Public License v1.0" + NL + " * which accompanies this distribution, and is available at" + NL + " * http://www.eclipse.org/legal/epl-v10.html" + NL + " * " + NL + " * Contributors: " + NL + " *     itemis - Initial API and implementation" + NL + " * " + NL + " * </copyright>";
  protected final String TEXT_5 = NL + " */" + NL + "package ";
  protected final String TEXT_6 = ";" + NL;
  protected final String TEXT_7 = NL + NL + NL + "public class ";
  protected final String TEXT_8 = " extends ";
  protected final String TEXT_9 = " {" + NL + "\t" + NL + "\tprotected class ";
  protected final String TEXT_10 = "<";
  protected final String TEXT_11 = ">> {" + NL + "" + NL + "\t\tprotected ";
  protected final String TEXT_12 = " type;" + NL + "" + NL + "\t\tpublic ";
  protected final String TEXT_13 = " (EClassifier type) {" + NL + "\t\t\t";
  protected final String TEXT_14 = ".isNotNull(type);" + NL + "\t\t\tthis.type = type;" + NL + "\t\t}" + NL;
  protected final String TEXT_15 = NL;
  protected final String TEXT_16 = NL + "    @Override";
  protected final String TEXT_17 = NL + "\t\tpublic List<EStructuralFeature> caseEAXML(";
  protected final String TEXT_18 = " anEAXML) {" + NL + "\t\t\tif (type instanceof ";
  protected final String TEXT_19 = ") {" + NL + "\t\t\t\tif (";
  protected final String TEXT_20 = ".eINSTANCE.getEAPackage().isSuperTypeOf(" + NL + "\t\t\t\t\t\t(EClass) type)) {" + NL + "\t\t\t\t\treturn ";
  protected final String TEXT_21 = NL + "\t\t\t\t\t\t\t.asList((EStructuralFeature) ";
  protected final String TEXT_22 = ");" + NL + "\t\t\t\t}" + NL + "\t\t\t\t";
  protected final String TEXT_23 = NL + "\t\t\t\tif (";
  protected final String TEXT_24 = ".eINSTANCE.getEAPackageableElement()" + NL + "\t\t\t\t\t\t.isSuperTypeOf((EClass) type)) {" + NL + "\t\t\t\t\treturn Arrays" + NL + "\t\t\t\t\t\t\t.asList((EStructuralFeature) ";
  protected final String TEXT_25 = NL + "\t\t\t}" + NL + "\t\t\treturn null;" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_26 = NL + "\t\tpublic List<EStructuralFeature> caseEAPackage(";
  protected final String TEXT_27 = " anEAPackage) {" + NL + "\t\t\tif (type instanceof EClass) {" + NL + "\t\t\t\tif (";
  protected final String TEXT_28 = ".eINSTANCE.getEAPackageableElement()" + NL + "\t\t\t\t\t\t.isSuperTypeOf((EClass) type)) {" + NL + "\t\t\t\t\tList<EStructuralFeature> relevantStructuralFeatures = new ArrayList<EStructuralFeature>(" + NL + "\t\t\t\t\t\t\t2);" + NL + "\t\t\t\t\trelevantStructuralFeatures.add(";
  protected final String TEXT_29 = "); " + NL + "\t\t\t\t\trelevantStructuralFeatures.add(";
  protected final String TEXT_30 = ");" + NL + "\t\t\t\t\treturn relevantStructuralFeatures;" + NL + "\t\t\t\t}" + NL + "\t\t\t\t";
  protected final String TEXT_31 = NL + "\t\t\t}" + NL + "\t\t\treturn null;" + NL + "\t\t}";
  protected final String TEXT_32 = NL + "   @Override";
  protected final String TEXT_33 = NL + "\t\tpublic List<EStructuralFeature> caseEAPackageableElement(";
  protected final String TEXT_34 = " packageableElement) {" + NL + "\t\t\tif (type instanceof EClass) {" + NL + "\t\t\t\tif (";
  protected final String TEXT_35 = ".eINSTANCE.getEAPackageableElement()" + NL + "\t\t\t\t\t\t.isSuperTypeOf((EClass) type)) {" + NL + "\t\t\t\t\treturn ";
  protected final String TEXT_36 = ".emptyList();" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t\treturn null;" + NL + "\t\t}";
  protected final String TEXT_37 = NL + "\t} // ";
  protected final String TEXT_38 = NL + "\t\t";
  protected final String TEXT_39 = NL + "  @Override";
  protected final String TEXT_40 = NL + "\tpublic List<EStructuralFeature> getFeaturesToTraverseFor(";
  protected final String TEXT_41 = " object," + NL + "\t\t\tEClassifier type) {" + NL + "\t\t";
  protected final String TEXT_42 = "TraveralSwitch traversalSwitch = new ";
  protected final String TEXT_43 = "TraveralSwitch(" + NL + "\t\t\t\ttype);" + NL + "\t\tList<EStructuralFeature> features = traversalSwitch.doSwitch(object);" + NL + "\t\tif (features != null) {" + NL + "\t\t\treturn features;" + NL + "\t\t}" + NL + "\t\treturn super.getFeaturesToTraverseFor(object, type);" + NL + "\t}" + NL + "\t";
  protected final String TEXT_44 = NL + "\tpublic boolean collectReachableObjectsOfTypeUnderObject(" + NL + "\t\t\t";
  protected final String TEXT_45 = "<EObject> result, EObject object, EClassifier type) {" + NL + "" + NL + "\t\t// Perform efficient direct search for instances of frequently used types" + NL + "\t\tif (type instanceof EClass && object instanceof EAXML) {" + NL + "\t\t\tEAXML eaxml = (EAXML) object;" + NL + "\t\t\t";
  protected final String TEXT_46 = NL + "\t\t\t// PackageableElements" + NL + "\t\t\tif (";
  protected final String TEXT_47 = ".eINSTANCE.getEAPackageableElement()" + NL + "\t\t\t\t\t.isSuperTypeOf((EClass) type)) {" + NL + "\t\t\t\tresult.addAll(getAllElementsOfTypeUnderEAXML(eaxml, type));" + NL + "\t\t\t\treturn true;" + NL + "\t\t\t}" + NL + "\t\t\t";
  protected final String TEXT_48 = NL + "\t\t}" + NL + "\t\treturn false;" + NL + "\t}";
  protected final String TEXT_49 = NL + "\tprivate ";
  protected final String TEXT_50 = "<EObject> getAllElementsOfTypeUnderEAXML(" + NL + "\t\t\tEAXML eaxml, EClassifier type) {" + NL + "\t\tCollection<EObject> result = new ";
  protected final String TEXT_51 = "<EObject>();" + NL + "\t\tfor (EAPackage eaPackage : eaxml.";
  protected final String TEXT_52 = "()) {" + NL + "\t\t\tresult.addAll(getAllElementsOfTypeInPackage(eaPackage, type));" + NL + "\t\t}" + NL + "\t\treturn result;" + NL + "\t}" + NL + "" + NL + "\tprivate ";
  protected final String TEXT_53 = "<EObject> getAllElementsOfTypeInPackage(" + NL + "\t\t\tEAPackage eaPackage, EClassifier type) {" + NL + "\t\tCollection<EObject> result = new ArrayList<EObject>();" + NL + "\t";
  protected final String TEXT_54 = NL + "\t\tfor (EAPackageableElement packageableElement : eaPackage.";
  protected final String TEXT_55 = "()) {" + NL + "\t\t\tif (type.isInstance(packageableElement)) {" + NL + "\t\t\t\tresult.add(packageableElement);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t";
  protected final String TEXT_56 = NL + "\t   for (";
  protected final String TEXT_57 = " eaElement : eaPackage.";
  protected final String TEXT_58 = "()) {" + NL + "\t\t\tif (type.isInstance(eaElement)) {" + NL + "\t\t\t\tresult.add(eaElement);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t";
  protected final String TEXT_59 = NL + "\t\tfor (EAPackage subPackage : eaPackage.";
  protected final String TEXT_60 = "()) {" + NL + "\t\t\tresult.addAll(getAllElementsOfTypeInPackage(subPackage, type));" + NL + "\t\t}" + NL + "\t\treturn result;" + NL + "\t}";
  protected final String TEXT_61 = NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

    GenPackage genPackage = (GenPackage)argument; GenModel genModel=genPackage.getGenModel();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_4);
    }}
    stringBuffer.append(TEXT_5);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getUtilitiesPackageName());
    stringBuffer.append(TEXT_6);
    genModel.addImport("org.eclipse.emf.ecore.EObject");
    genModel.addImport("org.eclipse.emf.ecore.EReference");
    genModel.markImportLocation(stringBuffer);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "TraversalHelper"));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(genModel.getImportedName("org.eclipse.sphinx.emf.ecore.DefaultEcoreTraversalHelper"));
    stringBuffer.append(TEXT_9);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "TraveralSwitch"));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(genModel.getImportedName(GenModels.getUtilityClassName(genModel, "Switch")));
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_11);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClassifier"));
    stringBuffer.append(TEXT_12);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "TraveralSwitch"));
    stringBuffer.append(TEXT_13);
    stringBuffer.append(genModel.getImportedName("org.eclipse.core.runtime.Assert"));
    stringBuffer.append(TEXT_14);
    GenClass packageableElementGenClass = GenModels.findGenClass(genModel, "EAPackageableElement");
    GenClass eaElementGenClass = GenModels.findGenClass(genModel, "EAElement");
    GenClass eaxmlGenClass = GenModels.findGenClass(genModel, "EAXML");
    GenClass eaPackageGenClass = GenModels.findGenClass(genModel, "EAPackage");
    stringBuffer.append(TEXT_15);
    if(eaPackageGenClass != null && eaxmlGenClass != null){
    GenFeature eaPackagePackagesGenFeature = GenClasses.getGenFeature(eaPackageGenClass, "subPackage");
    GenFeature eaPackageElementsGenFeature = GenClasses.getGenFeature(eaPackageGenClass, "element");
    GenFeature eaxmlPackagesGenFeature = GenClasses.getGenFeature(eaxmlGenClass, "topLevelPackage");
    if(genModel.useClassOverrideAnnotation())
    stringBuffer.append(TEXT_16);
    
    stringBuffer.append(TEXT_17);
    stringBuffer.append(eaxmlGenClass.getImportedInterfaceName());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_19);
    stringBuffer.append(genModel.getImportedName(eaPackageGenClass.getGenPackage().getInterfacePackageName()+"."+eaPackageGenClass.getGenPackage().getImportedPackageInterfaceName()));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(genModel.getImportedName("java.util.Arrays"));
    stringBuffer.append(TEXT_21);
    stringBuffer.append(eaxmlPackagesGenFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_22);
    if(packageableElementGenClass != null){
    stringBuffer.append(TEXT_23);
    stringBuffer.append(genModel.getImportedName(packageableElementGenClass.getGenPackage().getInterfacePackageName()+"."+packageableElementGenClass.getGenPackage().getImportedPackageInterfaceName()));
    stringBuffer.append(TEXT_24);
    stringBuffer.append(eaxmlPackagesGenFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_22);
    }
    stringBuffer.append(TEXT_25);
    if(genModel.useClassOverrideAnnotation())
    stringBuffer.append(TEXT_16);
    
    stringBuffer.append(TEXT_26);
    stringBuffer.append(eaPackageGenClass.getImportedInterfaceName());
    stringBuffer.append(TEXT_27);
    stringBuffer.append(genModel.getImportedName(eaPackageGenClass.getGenPackage().getInterfacePackageName()+"."+eaPackageGenClass.getGenPackage().getImportedPackageInterfaceName()));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(genModel.getImportedName("java.util.Arrays"));
    stringBuffer.append(TEXT_21);
    stringBuffer.append(eaPackagePackagesGenFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_22);
    if(packageableElementGenClass != null){
    stringBuffer.append(TEXT_23);
    stringBuffer.append(genModel.getImportedName(packageableElementGenClass.getGenPackage().getInterfacePackageName()+"."+packageableElementGenClass.getGenPackage().getImportedPackageInterfaceName()));
    stringBuffer.append(TEXT_28);
    stringBuffer.append(eaPackageElementsGenFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_29);
    stringBuffer.append(eaPackagePackagesGenFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_30);
    }
    stringBuffer.append(TEXT_31);
    }
    stringBuffer.append(TEXT_15);
    if(packageableElementGenClass != null){
    if(genModel.useClassOverrideAnnotation())
    stringBuffer.append(TEXT_32);
    
    stringBuffer.append(TEXT_33);
    stringBuffer.append(packageableElementGenClass.getImportedInterfaceName());
    stringBuffer.append(TEXT_34);
    stringBuffer.append(genModel.getImportedName(packageableElementGenClass.getGenPackage().getInterfacePackageName()+"."+packageableElementGenClass.getGenPackage().getImportedPackageInterfaceName()));
    stringBuffer.append(TEXT_35);
    stringBuffer.append(genModel.getImportedName("java.util.Collections"));
    stringBuffer.append(TEXT_36);
    }
    stringBuffer.append(TEXT_37);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "TraveralSwitch"));
    stringBuffer.append(TEXT_38);
    if(genModel.useClassOverrideAnnotation())
    stringBuffer.append(TEXT_39);
    
    stringBuffer.append(TEXT_40);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_41);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getPrefix());
    stringBuffer.append(TEXT_42);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getPrefix());
    stringBuffer.append(TEXT_43);
    if(genModel.useClassOverrideAnnotation())
    stringBuffer.append(TEXT_39);
    
    stringBuffer.append(TEXT_44);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_45);
    if(packageableElementGenClass != null){
    stringBuffer.append(TEXT_46);
    stringBuffer.append(genModel.getImportedName(packageableElementGenClass.getGenPackage().getInterfacePackageName()+"."+packageableElementGenClass.getGenPackage().getImportedPackageInterfaceName()));
    stringBuffer.append(TEXT_47);
    }
    stringBuffer.append(TEXT_48);
    if(eaxmlGenClass != null && eaPackageGenClass != null){
    GenFeature eaxmlPackagesGenFeature = GenClasses.getGenFeature(eaxmlGenClass, "topLevelPackage");
    GenFeature eaPackagePackagesGenFeature = GenClasses.getGenFeature(eaPackageGenClass, "subPackage");
    GenFeature eaPackageElementsGenFeature = GenClasses.getGenFeature(eaPackageGenClass, "element");
    stringBuffer.append(TEXT_49);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_50);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_51);
    stringBuffer.append(eaxmlPackagesGenFeature.getGetAccessor());
    stringBuffer.append(TEXT_52);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_53);
    if(packageableElementGenClass != null){
    stringBuffer.append(TEXT_54);
    stringBuffer.append(eaPackageElementsGenFeature.getGetAccessor());
    stringBuffer.append(TEXT_55);
    } else if(eaElementGenClass != null) {
    stringBuffer.append(TEXT_56);
    stringBuffer.append(eaElementGenClass.getImportedInterfaceName());
    stringBuffer.append(TEXT_57);
    stringBuffer.append(eaPackageElementsGenFeature.getGetAccessor());
    stringBuffer.append(TEXT_58);
    }
    stringBuffer.append(TEXT_59);
    stringBuffer.append(eaPackagePackagesGenFeature.getGetAccessor());
    stringBuffer.append(TEXT_60);
    }
    stringBuffer.append(TEXT_61);
    genModel.emitSortedImports();
    return stringBuffer.toString();
  }
}
