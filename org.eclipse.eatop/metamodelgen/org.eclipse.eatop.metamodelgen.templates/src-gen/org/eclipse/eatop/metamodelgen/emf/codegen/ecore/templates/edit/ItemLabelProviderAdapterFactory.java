package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.edit;

import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;

public class ItemLabelProviderAdapterFactory
{
  protected static String nl;
  public static synchronized ItemLabelProviderAdapterFactory create(String lineSeparator)
  {
    nl = lineSeparator;
    ItemLabelProviderAdapterFactory result = new ItemLabelProviderAdapterFactory();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = "/**";
  protected final String TEXT_3 = NL + " * ";
  protected final String TEXT_4 = NL + " * <copyright>" + NL + " * " + NL + " * Copyright (c) 2014 itemis and others." + NL + " * All rights reserved. This program and the accompanying materials" + NL + " * are made available under the terms of the Eclipse Public License v1.0" + NL + " * which accompanies this distribution, and is available at" + NL + " * http://www.eclipse.org/legal/epl-v10.html" + NL + " * " + NL + " * Contributors: " + NL + " *     itemis - Initial API and implementation" + NL + " * " + NL + " * </copyright>";
  protected final String TEXT_5 = NL + " */" + NL + "package ";
  protected final String TEXT_6 = ";" + NL;
  protected final String TEXT_7 = NL + NL + "/**" + NL + " * This is the item label provider adapter factory for a ";
  protected final String TEXT_8 = " release." + NL + " * <!-- begin-user-doc -->" + NL + " * <!-- end-user-doc -->" + NL + " * @generated" + NL + " */" + NL + "public class ItemLabelProviderAdapterFactory implements IAdapterFactory {" + NL + "" + NL + "\t/** <!-- begin-user-doc -->" + NL + "\t *  <!-- end-user-doc -->" + NL + "\t *  @see org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object, java.lang.Class)" + NL + "\t *  @generated" + NL + "\t */" + NL + "\tpublic Object getAdapter(Object adaptableObject, Class adapterType) {" + NL + "\t\tif (adapterType.equals(IItemLabelProvider.class)) {" + NL + "\t\t\t// IItemLabelProvider adapter for ";
  protected final String TEXT_9 = "?" + NL + "\t\t\tif (adaptableObject instanceof ";
  protected final String TEXT_10 = ") {" + NL + "\t\t\t\treturn new ";
  protected final String TEXT_11 = "();" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\treturn null;" + NL + "\t}" + NL + "    /** <!-- begin-user-doc -->" + NL + "\t *  <!-- end-user-doc -->" + NL + "\t *  @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()" + NL + "\t *  @generated" + NL + "\t */" + NL + "\tpublic Class[] getAdapterList() {" + NL + "\t\treturn new Class<?>[] { IItemLabelProvider.class };" + NL + "\t}" + NL + "}";
  protected final String TEXT_12 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

    GenPackage genPackage = (GenPackage)argument; GenModel genModel=genPackage.getGenModel();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_4);
    }}
    stringBuffer.append(TEXT_5);
    stringBuffer.append(genModel.getEditPluginPackageName());
    stringBuffer.append(TEXT_6);
    genModel.addImport("org.eclipse.core.runtime.IAdapterFactory");
    genModel.addImport("org.eclipse.emf.edit.provider.IItemLabelProvider");
    genModel.markImportLocation(stringBuffer);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(genModel.getModelName());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "ReleaseDescriptor"));
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genModel.getImportedName(GenModels.getUtilityClassName(genModel,"ReleaseDescriptor")));
    stringBuffer.append(TEXT_10);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel, "ReleaseDescriptorItemLabelProvider"));
    stringBuffer.append(TEXT_11);
    genModel.emitSortedImports();
    stringBuffer.append(TEXT_12);
    return stringBuffer.toString();
  }
}
