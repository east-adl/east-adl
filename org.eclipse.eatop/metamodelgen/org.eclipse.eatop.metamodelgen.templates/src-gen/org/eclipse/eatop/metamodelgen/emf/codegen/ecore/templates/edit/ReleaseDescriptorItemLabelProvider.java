package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.edit;

import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;

public class ReleaseDescriptorItemLabelProvider
{
  protected static String nl;
  public static synchronized ReleaseDescriptorItemLabelProvider create(String lineSeparator)
  {
    nl = lineSeparator;
    ReleaseDescriptorItemLabelProvider result = new ReleaseDescriptorItemLabelProvider();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = "/**";
  protected final String TEXT_3 = NL + " * ";
  protected final String TEXT_4 = NL + " * <copyright>" + NL + " * " + NL + " * Copyright (c) 2014 itemis and others." + NL + " * All rights reserved. This program and the accompanying materials" + NL + " * are made available under the terms of the Eclipse Public License v1.0" + NL + " * which accompanies this distribution, and is available at" + NL + " * http://www.eclipse.org/legal/epl-v10.html" + NL + " * " + NL + " * Contributors: " + NL + " *     itemis - Initial API and implementation" + NL + " * " + NL + " * </copyright>";
  protected final String TEXT_5 = NL + " */" + NL + "package ";
  protected final String TEXT_6 = ";" + NL;
  protected final String TEXT_7 = NL + NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * <!-- end-user-doc -->" + NL + " * @generated" + NL + " */" + NL + "public class ";
  protected final String TEXT_8 = " implements IItemLabelProvider {" + NL + "" + NL + "\tprivate static String IMG_OVR_";
  protected final String TEXT_9 = " = \"full/ovr16/";
  protected final String TEXT_10 = "_ovr\";" + NL + "\t" + NL + "\t/**" + NL + "     * <!-- begin-user-doc -->" + NL + "     * <!-- end-user-doc -->" + NL + "     * @see org.eclipse.emf.edit.provider.IItemLabelProvider#getImage(java.lang.Object)" + NL + "     * @generated" + NL + "     */" + NL + "\tpublic Object getImage(Object object) {" + NL + "\t\tif (object instanceof ";
  protected final String TEXT_11 = ") {" + NL + "\t\t\treturn Activator.INSTANCE.getImage(IMG_OVR_";
  protected final String TEXT_12 = ");" + NL + "\t\t}" + NL + "\t\treturn null;" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "     * <!-- begin-user-doc -->" + NL + "     * <!-- end-user-doc -->" + NL + "\t * @see org.eclipse.emf.edit.provider.IItemLabelProvider#getText(java.lang.Object)" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic String getText(Object object) {" + NL + "\t\tif (object instanceof ";
  protected final String TEXT_13 = ") {" + NL + "\t\t\treturn ((";
  protected final String TEXT_14 = ") object).getName();" + NL + "\t\t}" + NL + "\t\treturn null;" + NL + "\t}" + NL + "}";
  protected final String TEXT_15 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 * 
 * Copyright (c) Continental AG and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 * 
 * Contributors: 
 *     Continental AG - Initial API and implementation
 * 
 * </copyright>
 */

    GenPackage genPackage = (GenPackage)argument; GenModel genModel=genPackage.getGenModel();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_4);
    }}
    stringBuffer.append(TEXT_5);
    stringBuffer.append(genModel.getEditPluginPackageName());
    stringBuffer.append(TEXT_6);
    genModel.addImport("org.eclipse.core.runtime.IAdapterFactory");
    genModel.addImport("org.eclipse.emf.edit.provider.IItemLabelProvider");
    genModel.markImportLocation(stringBuffer);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel,"ReleaseDescriptorItemLabelProvider" ));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(genModel.getModelName().toUpperCase());
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genModel.getModelName());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genModel.getImportedName(GenModels.getUtilityClassName(genModel, "ReleaseDescriptor")));
    stringBuffer.append(TEXT_11);
    stringBuffer.append(genModel.getModelName().toUpperCase());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel,"ReleaseDescriptor" ));
    stringBuffer.append(TEXT_13);
    stringBuffer.append(GenModels.getUtilityClassSimpleName(genModel,"ReleaseDescriptor" ));
    stringBuffer.append(TEXT_14);
    genModel.emitSortedImports();
    stringBuffer.append(TEXT_15);
    return stringBuffer.toString();
  }
}
