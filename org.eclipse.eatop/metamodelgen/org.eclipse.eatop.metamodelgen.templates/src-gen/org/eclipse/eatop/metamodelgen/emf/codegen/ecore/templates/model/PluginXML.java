package org.eclipse.eatop.metamodelgen.emf.codegen.ecore.templates.model;

import java.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenModels;
import org.eclipse.eatop.metamodelgen.templates.internal.util.EASTADLGenModels;
import org.eclipse.eatop.metamodelgen.templates.source.*;
import org.eclipse.eatop.metamodelgen.templates.internal.util.GenPackages;

public class PluginXML
{
  protected static String nl;
  public static synchronized PluginXML create(String lineSeparator)
  {
    nl = lineSeparator;
    PluginXML result = new PluginXML();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + NL + "<?eclipse version=\"3.0\"?>" + NL;
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = "<!--";
  protected final String TEXT_4 = NL + " ";
  protected final String TEXT_5 = NL + "   <copyright>" + NL + "  " + NL + "  Copyright (c) 2014 itemis and others." + NL + "  All rights reserved. This program and the accompanying materials" + NL + "  are made available under the terms of the Eclipse Public License v1.0" + NL + "  which accompanies this distribution, and is available at" + NL + "  http://www.eclipse.org/legal/epl-v10.html" + NL + "  " + NL + "  Contributors: " + NL + "      itemis - Initial API and implementation" + NL + "  " + NL + "  </copyright>";
  protected final String TEXT_6 = NL + "-->" + NL;
  protected final String TEXT_7 = NL + "<plugin>";
  protected final String TEXT_8 = NL + "<plugin" + NL + "      name=\"%pluginName\"" + NL + "      id=\"";
  protected final String TEXT_9 = "\"" + NL + "      version=\"0.5.0\"";
  protected final String TEXT_10 = NL + "      provider-name=\"%providerName\"" + NL + "      class=\"";
  protected final String TEXT_11 = "$Implementation\">";
  protected final String TEXT_12 = NL + "      provider-name=\"%providerName\">";
  protected final String TEXT_13 = NL + NL + "   <requires>";
  protected final String TEXT_14 = NL + "      <import plugin=\"";
  protected final String TEXT_15 = "\"";
  protected final String TEXT_16 = " export=\"true\"";
  protected final String TEXT_17 = "/>";
  protected final String TEXT_18 = NL + "   </requires>" + NL + "" + NL + "   <runtime>";
  protected final String TEXT_19 = NL + "      <library name=\"";
  protected final String TEXT_20 = ".jar\">";
  protected final String TEXT_21 = NL + "      <library name=\".\">";
  protected final String TEXT_22 = NL + "         <export name=\"*\"/>" + NL + "      </library>" + NL + "   </runtime>";
  protected final String TEXT_23 = NL + "     ";
  protected final String TEXT_24 = NL + "     <extension point=\"org.eclipse.emf.ecore.generated_package\">" + NL + "      <package" + NL + "            uri=\"";
  protected final String TEXT_25 = NL + "            class=\"";
  protected final String TEXT_26 = "\"" + NL + "            genModel=\"";
  protected final String TEXT_27 = "\"/>";
  protected final String TEXT_28 = NL + "     </extension>";
  protected final String TEXT_29 = NL + "     <extension" + NL + "         point=\"org.eclipse.sphinx.emf.metaModelDescriptors\">" + NL + "       <descriptor" + NL + "            id=\"";
  protected final String TEXT_30 = "\"" + NL + "            class=\"";
  protected final String TEXT_31 = ".";
  protected final String TEXT_32 = "ReleaseDescriptor\">" + NL + "       </descriptor>" + NL + "     </extension>     " + NL + "     ";
  protected final String TEXT_33 = NL + "   <extension point=\"org.eclipse.emf.ecore.generated_package\">" + NL + "      <package" + NL + "            uri=\"";
  protected final String TEXT_34 = NL + "   </extension>";
  protected final String TEXT_35 = NL + "   <extension point=\"org.eclipse.emf.ecore.content_parser\">" + NL + "      <parser" + NL + "            contentTypeIdentifier=\"";
  protected final String TEXT_36 = "\"/>" + NL + "   </extension>";
  protected final String TEXT_37 = NL + NL + "  " + NL + "   <extension point=\"org.eclipse.core.contenttype.contentTypes\">" + NL + "      <content-type" + NL + "            base-type=\"";
  protected final String TEXT_38 = "\"" + NL + "            file-extensions=\"";
  protected final String TEXT_39 = "\"" + NL + "            id=\"";
  protected final String TEXT_40 = "\"" + NL + "            name=\"%_UI_";
  protected final String TEXT_41 = "_content_type\"" + NL + "            priority=\"normal\">" + NL + "         <describer class=\"org.eclipse.emf.ecore.xmi.impl.RootXMLContentHandlerImpl$Describer\">";
  protected final String TEXT_42 = NL + "\t\t<parameter name=\"namespacePattern\" value=\"";
  protected final String TEXT_43 = "\"/>    " + NL + "\t";
  protected final String TEXT_44 = NL + "            <parameter name=\"kind\" value=\"xmi\"/>";
  protected final String TEXT_45 = NL + "         </describer>" + NL + "      </content-type>" + NL + "   </extension>" + NL + "" + NL + "   ";
  protected final String TEXT_46 = NL + NL + "   <extension point=\"org.eclipse.emf.ecore.extension_parser\">" + NL + "      <parser" + NL + "            type=\"";
  protected final String TEXT_47 = "  ";
  protected final String TEXT_48 = NL + "     " + NL + "  <extension" + NL + "    point=\"org.eclipse.sphinx.emf.validation.registration\">" + NL + "    <model" + NL + "      NsURI=\"";
  protected final String TEXT_49 = "\"" + NL + "      class=\"";
  protected final String TEXT_50 = "\"" + NL + "      filter=\"";
  protected final String TEXT_51 = "\"" + NL + "      Name=\"";
  protected final String TEXT_52 = "\"" + NL + "      id=\"";
  protected final String TEXT_53 = "\">" + NL + "    </model>" + NL + "   </extension>" + NL + "" + NL + "</plugin>";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * <copyright>
 *
 * Copyright (c) 2002-2005 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: 
 *   IBM - Initial API and implementation
 *
 * </copyright>
 */

    GenModel genModel = (GenModel)argument; /* Trick to import java.util.* without warnings */Iterator.class.getName();
    GenPackage rootGenPackage = GenModels.getRootGenPackage(genModel);
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_4);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_5);
    }}
    stringBuffer.append(TEXT_6);
    if (genModel.isBundleManifest()) {
    stringBuffer.append(TEXT_7);
    } else {
    stringBuffer.append(TEXT_8);
    stringBuffer.append(genModel.getModelPluginID());
    stringBuffer.append(TEXT_9);
    if (genModel.hasModelPluginClass()) { 
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genModel.getQualifiedModelPluginClassName());
    stringBuffer.append(TEXT_11);
    } else {
    stringBuffer.append(TEXT_12);
    }
    stringBuffer.append(TEXT_13);
    for (String pluginID : genModel.getModelRequiredPlugins()) {
    stringBuffer.append(TEXT_14);
    stringBuffer.append(pluginID);
    stringBuffer.append(TEXT_15);
    if (!pluginID.startsWith("org.eclipse.core.runtime")) {
    stringBuffer.append(TEXT_16);
    }
    stringBuffer.append(TEXT_17);
    }
    stringBuffer.append(TEXT_18);
    if (genModel.isRuntimeJar()) {
    stringBuffer.append(TEXT_19);
    stringBuffer.append(genModel.getModelPluginID());
    stringBuffer.append(TEXT_20);
    } else {
    stringBuffer.append(TEXT_21);
    }
    stringBuffer.append(TEXT_22);
    }
    
List<GenPackage> genPackagesWithClassifiers = genModel.getAllGenPackagesWithClassifiers();

    for (GenPackage genPackage : genPackagesWithClassifiers) {
    if((genPackage == GenModels.getRootGenPackage(genModel))) {
    stringBuffer.append(TEXT_23);
    if (EASTADLGenModels.hasRootUtilitiesGenPackage(genModel)) {
    stringBuffer.append(TEXT_24);
    stringBuffer.append(EASTADLGenModels.getEastADLReleaseNamespace(genModel));
    stringBuffer.append(TEXT_15);
    if (genModel.hasLocalGenModel()) {
    stringBuffer.append(TEXT_25);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_26);
    stringBuffer.append(genModel.getRelativeGenModelLocation());
    stringBuffer.append(TEXT_27);
    } else {
    stringBuffer.append(TEXT_25);
    stringBuffer.append(GenModels.getRootGenPackage(genModel).getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_27);
    }
    stringBuffer.append(TEXT_28);
    }
    stringBuffer.append(TEXT_23);
    if (EASTADLGenModels.hasRootUtilitiesGenPackage(genModel)) {
    stringBuffer.append(TEXT_29);
    stringBuffer.append(EASTADLGenModels.getEastADLMMDescriptorID(genModel));
    stringBuffer.append(TEXT_30);
    stringBuffer.append(rootGenPackage.getUtilitiesPackageName());
    stringBuffer.append(TEXT_31);
    stringBuffer.append(rootGenPackage.getPrefix());
    stringBuffer.append(TEXT_32);
    }
    } else {
    stringBuffer.append(TEXT_33);
    stringBuffer.append(genPackage.getNSURI());
    stringBuffer.append(TEXT_15);
    if (genModel.hasLocalGenModel()) {
    stringBuffer.append(TEXT_25);
    stringBuffer.append(genPackage.getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_26);
    stringBuffer.append(genModel.getRelativeGenModelLocation());
    stringBuffer.append(TEXT_27);
    } else {
    stringBuffer.append(TEXT_25);
    stringBuffer.append(genPackage.getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_27);
    }
    stringBuffer.append(TEXT_34);
    }
    if (genPackage.isContentType()) {
    stringBuffer.append(TEXT_2);
    if (EASTADLGenModels.hasRootUtilitiesGenPackage(genModel)) {
    stringBuffer.append(TEXT_35);
    stringBuffer.append(genPackage.getContentTypeIdentifier());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(genPackage.getQualifiedEffectiveResourceFactoryClassName());
    stringBuffer.append(TEXT_36);
    }
    stringBuffer.append(TEXT_37);
    stringBuffer.append(GenPackages.getBaseContentType(genModel, genPackage));
    stringBuffer.append(TEXT_38);
    stringBuffer.append(EASTADLGenModels.getFileExtension(genModel));
    stringBuffer.append(TEXT_39);
    stringBuffer.append(genPackage.getContentTypeIdentifier());
    stringBuffer.append(TEXT_40);
    stringBuffer.append(genPackage.getPrefix());
    stringBuffer.append(TEXT_41);
    if (genPackage == GenModels.getRootGenPackage(genModel)){
    stringBuffer.append(TEXT_42);
    stringBuffer.append(EASTADLGenModels.getNamespacePattern(genModel));
    stringBuffer.append(TEXT_43);
    }
    if (genPackage.isXMIResource()) {
    stringBuffer.append(TEXT_44);
    }
    stringBuffer.append(TEXT_45);
    } else if (genPackage.getResource() != GenResourceKind.NONE_LITERAL) {
    stringBuffer.append(TEXT_46);
    stringBuffer.append(genPackage.getFileExtension());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(genPackage.getQualifiedResourceFactoryClassName());
    stringBuffer.append(TEXT_36);
    }
    stringBuffer.append(TEXT_47);
    }
    stringBuffer.append(TEXT_48);
    stringBuffer.append(GenModels.createValidationNSURI(genModel));
    stringBuffer.append(TEXT_49);
    stringBuffer.append(GenModels.createValidationClass(genModel));
    stringBuffer.append(TEXT_50);
    stringBuffer.append(EASTADLGenModels.createValidationFilter(genModel));
    stringBuffer.append(TEXT_51);
    stringBuffer.append(EASTADLGenModels.createValidationName(genModel));
    stringBuffer.append(TEXT_52);
    stringBuffer.append(GenModels.createValidationId(genModel));
    stringBuffer.append(TEXT_53);
    stringBuffer.append(TEXT_2);
    return stringBuffer.toString();
  }
}
