# General Information
The code in this directory is a condensed and adapted variant of the code in the sibling directory <code>metamodelgen</code>, where some code portions are simply copied and partially adapted.
Thus, the copyright for the copied code portions remains the same, as also can be seen from the corresponding copyright headers.

The code in <code>metamodelgen</code> aims at generating an .ecore metamodel from a metamodel in Enterprise Architect and generating further artifacts (.genmodel, .xsd, and model/edit code).

In contrast, the code in this directory aims at only generating the further artifacts mentioned above from an existing .ecore metamodel without having the overhead of having a dedicated Enterprise Architect adapter.
We opted for this approach of copying and adapting selected code portions from <code>metamodelgen</code>, as extending and subclassing this code would have resulted in an awkward code design.

# Documenting and Evolving Ecore-based EAST-ADL Metamodels
Instead of documenting the EAST-ADL metamodels in Enterprise Architect, you will do it in Ecore directly in this approach.
For this purpose, you will apply the facilities of <code>EcoreTools</code> that EMF and an Eclipse Modelling Tools ships with.
To get familar with it, please consider this [EcoreTools documentation](https://www.eclipse.org/ecoretools/doc/).

## Setting up the Modelling and Documentation Environment
As the target definition for the generation and EATOP environment is very restricted and does not contain the <code>EcoreTools</code>, you have to use a dedicated Eclipse for maintaining, evolving, and documenting the EAST-ADL metamodel:
1. Get an Eclipse Modelling Tools
2. Import from <code>org.eclipse.eatop/plugins</code> the project <code>org.eclipse.eatop.geastadl</code> (do not care for the dependency error)
3. Import from <code>org.eclipse.eatop/genmodelcodegen</code> the project <code>org.eclipse.eatop.eastadl.documentation</code>

The latter project should only contain a folder <code>model</code> with all versions of EAST-ADL metamodels, which means it shall only contain plain <code>.ecore</code> files and the corresponding <code>.aird</code> files for the metamodels' visualization and documentation. 
Any of these versions has then to be copied into a new empty plugin, from which the remaining artifacts including the model/edit code will be generated for the application in EATOP (see below).
This copying procedure is important, as the generation will also post-process the <code>.ecore</code> file and, e.g., flatten the package hierarchy, potentially yielding the loss of the diagrams.
Thus, it is important to maintain the structured metamodels containing the visualization/documentation etc. in a dedicated project, and to copy them into further plugins that serve only the purpose of being integrated in EATOP.

## Adding / Maintaining Diagrams and Documentation
1. Open in the folder <code>model</code> the <code>eastadl\<version>.aird</code> of your choice (initially, it only contains the <code>eastadl22.aird</code> and <code>eastadl22.ecore</code>).
2. Optionally, if you start with an <code>.ecore</code> file that has no <code>.aird</code> file associated with it: Right-click on the <code>.ecore</code> file and select "Initialize Ecore Diagram...".
3. Create/edit diagrams for the specific metamodel packages in package-based <i>class diagrams</i>, and add/modify documentation with package-based <i>documentation tables</i>. Furthermore, <code>EcoreTools</code> provides many other visualization and documentation possibilities ([EcoreTools documentation](https://www.eclipse.org/ecoretools/doc/)).

## Adding new Metaclasses
1. Open in the folder <code>model</code> the <code>eastadl\<version>.ecore</code> of your choice (initially, it only contains the <code>eastadl22.ecore</code>).
2. Add a new metaclass by right-clicking on any package in the metamodel and select "New Child" -> "EClass". 
Specify it's name, attributes, and references.
You can also edit the metamodel via [class diagrams](https://www.vogella.com/tutorials/EclipseEMF/article.html) (cf. also [EcoreTools documentation](https://www.eclipse.org/ecoretools/doc/)).
A similar procedure applies for the addition of new datatypes.
For the full inclusion into the EAST-ADL language infrastructure, be sure to set the corresponding super-types like <code>EAElement</code> or <code>EAPackagableElement</code>.
3. For any of these newly created metaclasses, you have to add metadata annotations (<i>EAnnotations</i>) that are important for certain EATOP features. 
    - This can be done via the two options:
        - In the <code>.aird</code> file, open/create a documentation table for the corresponding package. Click on the newly created metaclass, and got to the tab "Ecore" -> "Annotation". Here, you can add new EAnnotations (see below).
        - In the <code>.ecore</code> file, right-click on the the newly created metaclass and select "New Child" -> "EAnnotation". The submenu of "EAnnotation" allows the possibilities described below.
    - The EAnnotations to specify are (as not everything can be documented completely here, please refer to EAnnotations of already existing metaclasses): 
        - For displaying the documentation of the metaclass in EATOP: "EAnnotation - http://www.eclipse.org/emf/2002/GenModel". Specify the <code>value</code> for the <code>key</code> "documentation". 
        - Optionally: In Enterprise Architect, certain stereotypes seem to play a role. If there should be specified such stereotype information, add it via an "EAnnotation - Stereotype". For the <code>key</code> "Stereotype", specify the name of the stereotype as <code>value</code> (e.g., "atpStructureElement" for VehicleLevel).


# Generating the Model/Edit Code Plugins for EATOP from an Ecore-based EAST-ADL Metamodel in a Specific Version

## Setting up the Development Workspace
1. Get an Eclipse Modelling Tools
2. Install the Xtend SDK
3. Import project <code>org.eclipse.eatop/tools/org.eclipse.eatop.targets/2019-12</code> and load target definition <code>2019-12.target</code> (or 2023-03)
4. Import the projects...
    - ...from <code>org.eclipse.eatop/metamodelgen</code> (sometimes, multiple project refreshs/cleanups and Eclipse restarts are required to get these plugins running with the Xtend SDK and the target definition):
        - <code>org.eclipse.eatop.metamodelgen.serialization.generators</code>
        - <code>org.eclipse.eatop.metamodelgen.serialization.source</code>
        - <code>org.eclipse.eatop.metamodelgen.serialization.templates</code>
    - ...from <code>org.eclipse.eatop/genmodelcodegen</code>:
        - <code>org.eclipse.eatop.genmodelcodegen</code>
        - <code>org.eclipse.eatop.genmodelcodegen.ui</code>
5. If you increase the EAST-ADL version number: In the plugin <code>org.eclipse.eatop.metamodelgen.serialization.generators</code>, adapt the class <code>org.eclipse.eatop.metamodelgen.serialization.generators.util.IEASTADLGeneratorConstants</code> by increasing the version number of it's field <code>EASTADL_VERSION</code>. Probably, you also have to go to the plugin <code>org.eclipse.eatop.metamodelgen.serialization.source</code> and add to its a folder <code>model</code> a new folder <code>eastadl-\<version></code> with a corresponding file <code>config.properties</code>.
6. Run the plugins as Eclipse application

## Setting up the Runtime Workspace
1. Import the projects from <code>org.eclipse.eatop/plugins</code>:
    - <code>org.eclipse.eatop.common</code>  
    - <code>org.eclipse.eatop.geastadl</code>
    - <code>org.eclipse.eatop.geastadl.edit</code>
    - <code>org.eclipse.eatop.serialization</code>
2. Create an empty Java project <code>org.eclipse.eatop.eastadl\<majorVersion>\<minorVersion>[\<patchVersion>]</code> (e.g., <code>org.eclipse.eatop.eastadl22</code>), preferrably with JavaSE-1.6 as execution environment JRE (fitting to the execution environment JRE of the other EATOP plugins)
3. Create in this plugin a new folder <code>model</code> and copy from <code>org.eclipse.eatop.eastadl.documentation</code> (see above) an EAST-ADL Ecore metamodel with the name <code>eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].ecore</code> (e.g., <code>eastadl22.ecore</code>) into it. 
4. Open <code>eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].ecore</code> with a text editor, search for the string <code>geastadl.ecore#</code>, and either...
    - ...(recommended way) check that all corresponding references point to <code>../../org.eclipse.eatop.geastadl/model/geastadl.ecore#</code>. 
   If this is not the case, replace all occurrences with the text editor with this String. 
   Additionally, check that the plugin <code>org.eclipse.eatop.geastadl</code> resides in the file system as a sibling to the plugin <code>org.eclipse.eatop.eastadl\<majorVersion>\<minorVersion>[\<patchVersion>]</code> in the runtime workspace. 
   If this is not the case, re-import this plugin with the option "Copy projects into the workspace" enabled.
    - ...or check that all corresponding references point to <code>./geastadl.ecore#</code>. 
   If this is not the case, replace all occurrences with the text editor with this String. 
   Additionally, copy the files <code>geastadl.ecore</code> and <code>geastadl.genmodel</code> from <code>org.eclipse.eatop.geastadl/model</code> into the same folder than <code>eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].ecore</code>.
5. Right-click the metamodel, select <code>EATOP - Generate EAST-ADL Editing Infrastructure -> Generate EAST-ADL Genmodel, XSD, and Model/Edit Code Plugins</code>, and wait for the operation to finish. The generation will generate code and further artifacts for the plugin <code>org.eclipse.eatop.eastadl<majorVersion\><minorVersion\>\[<patchVersion\>\]</code> and additionally will generate a further plugin <code>org.eclipse.eatop.eastadl<majorVersion\><minorVersion\>\[<patchVersion\>\].edit</code>.
6. Optionally, overwrite/adapt the generated generic icon figures in <code>org.eclipse.eatop.eastadl\<majorVersion>\<minorVersion>[\<patchVersion>].edit/icons</code>.


## Setting up EATOP with the Generated Plugins
1. Import project <code>org.eclipse.eatop/tools/org.eclipse.eatop.targets/2019-12</code> and load target definition <code>2019-12.target</code> (or 2023-03)
2. Import the projects...
    - ...<code>org.artop.eel.common</code>
    - ...from <code>org.eclipse.eatop/plugins</code>:
        - <code>org.eclipse.eatop.common</code>
        - <code>org.eclipse.eatop.common.ui</code>
        - <code>org.eclipse.eatop.geastadl</code>
        - <code>org.eclipse.eatop.geastadl.edit</code>
        - <code>org.eclipse.eatop.sdk</code>
        - <code>org.eclipse.eatop.serialization</code>
        - <code>org.eclipse.eatop.workspace</code>
        - <code>org.eclipse.eatop.workspace.ui</code>
    - ...from <code>org.eclipse.eatop/examples</code>:
        - <code>org.eclipse.eatop.examples.actions</code>
        - <code>org.eclipse.eatop.examples.common.ui</code>
        - <code>org.eclipse.eatop.examples.editor</code>
        - <code>org.eclipse.eatop.examples.explorer</code>
        - optionally, further plugins like <code>org.eclipse.eatop.demonstrator</code> or graphical editors
    - ...from your runtime workspace, the freshly generated plugins
        - <code>org.eclipse.eatop.eastadl<majorVersion\><minorVersion\>\[<patchVersion\>\]</code>
        - <code>org.eclipse.eatop.eastadl<majorVersion\><minorVersion\>\[<patchVersion\>\].edit</code>
3. Run the plugins as Eclipse application
