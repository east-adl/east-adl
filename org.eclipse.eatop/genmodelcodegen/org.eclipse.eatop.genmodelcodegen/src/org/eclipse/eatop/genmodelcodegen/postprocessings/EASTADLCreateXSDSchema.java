/**
 * <copyright>
 *  
 * Copyright (c) 2014 itemis and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 *     itemis - Initial API and implementation
 *  
 * </copyright>
 * 
 */
package org.eclipse.eatop.genmodelcodegen.postprocessings;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.eatop.genmodelcodegen.messages.Messages;
import org.eclipse.eatop.genmodelcodegen.util.IConstants;
import org.eclipse.eatop.metamodelgen.serialization.generators.xsd.EASTADLEcore2XSDGenerator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.sphinx.emf.serialization.generators.xsd.Ecore2XSDGenerator;

public class EASTADLCreateXSDSchema extends PostProcessingTemplate {	
	protected IProgressMonitor monitor;	
	// Adaptations Joerg Holtmann:
	private IFile modelIFile;
	private String version;	
	
	public EASTADLCreateXSDSchema(IProgressMonitor monitor, IFile modelIFile, String version) {	
		this.monitor = monitor;
		// Adaptations Joerg Holtmann:
		this.modelIFile = modelIFile;
		this.version = version;
	}

	protected void createXSD() throws IOException {
		// Adaptations Joerg Holtmann:
		// replace *.ecore with *.xsd
		String xsdPath = this.modelIFile.getFullPath().toString().replace(IConstants.ECORE_FILE_POSTFIX, IConstants.XSD_FILE_POSTFIX);
		// Assumption: EAST-ADL version is specified as <majorVersion>.<minorVersion>.<patchVersion> 
		// (e.g., "2.2.0", cf. org.eclipse.eatop.metamodelgen.serialization.generators.util.IEASTADLGeneratorConstants#EASTADL_VERSION)
		// ==> replace the dots "." with a "-" (e.g., "2-2-0")		
		String xsdVersion = this.version.replaceAll("\\.", "-");
		// Assumption: modelIFile name contains the EAST-ADL version in the format <majorVersion><minorVersion><patchVersion>
		// directly in front of the filename postfix ".xsd" ==> replace this number with the XSD version format that is expected by EATOP
		// (e.g., "eastadl22.xsd" ==> "eastadl_2-2-0.xsd")
		xsdPath = xsdPath.replaceAll("[0-9]." + IConstants.XSD_FILE_POSTFIX, "_" + xsdVersion + IConstants.XSD_FILE_POSTFIX);
		
		URI xsdFileURI = URI.createPlatformResourceURI(xsdPath, true);

		// xtend ecore to xsd generator
		Ecore2XSDGenerator ecore2XSDGenerator = new EASTADLEcore2XSDGenerator(xsdFileURI, model);

		if (ecore2XSDGenerator != null) {
			ecore2XSDGenerator.run(monitor);
		}
	}
	
	@Override
	public void execute() {
		try {
			monitor.beginTask(Messages.task_CreateXSDSchema, 100);
			createXSD();
			monitor.done();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
}
