/**
 * <copyright>
 *  
 * Copyright (c) 2014 itemis and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 *     itemis - Initial API and implementation
 *  
 * </copyright>
 * 
 */
package org.eclipse.eatop.genmodelcodegen.postprocessings;

import java.text.MessageFormat;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class AddOriginalQualifiedNameAnnotation extends PostProcessingTemplate {

	private String GENMODEL_ANNOTATION_SOURCE = "http://www.eclipse.org/emf/2002/GenModel"; //$NON-NLS-1$
	private String DOCUMENTATION_ANNOTATION = "documentation"; //$NON-NLS-1$
	private final static String QUALIFIED_NAME_ID = "Original fully qualified name: ";
	private final static String QUALIFIED_NAME_MESSAGE = "\\n\\n" + QUALIFIED_NAME_ID + "\\n<em><b>{0}</b></em>"; // Messages.qualified_Name_Message
	private final static String EASTADL_PKG_NAME = "EAST-ADL.";

	@Override
	public void execute() {
		logger.info("AddOriginalQualifiedNameAnnotation started!"); // Messages.logger_AddOriginalQualifiedNameAnnotation
		EPackage rootPackage = (EPackage) EcoreUtil.getRootContainer(model);
		
		for (TreeIterator<EObject> iter = rootPackage.eAllContents(); iter.hasNext();) {
			EObject element = iter.next();
			
			if (element instanceof EClass || element instanceof EDataType) {
				EClassifier eClassifier = (EClassifier) element;
				String notes = EcoreUtil.getAnnotation(eClassifier, GENMODEL_ANNOTATION_SOURCE,
						DOCUMENTATION_ANNOTATION);
				String qualifiedName = getOriginalQualifiedName(eClassifier.getEPackage(), eClassifier.getName());
				String qualifiedNameMsg = MessageFormat.format(QUALIFIED_NAME_MESSAGE, qualifiedName);
				
				if(notes == null) {
					// prevent null String
					notes = "";
				}

				if (!notes.contains(QUALIFIED_NAME_ID)) {
					// no qualified name annotation at all ==> the metaclass/datatype at hand is a
					// new one, so that it's fully qualified path has to be appended to the notes.
					EcoreUtil.setAnnotation(eClassifier, GENMODEL_ANNOTATION_SOURCE, DOCUMENTATION_ANNOTATION,
							notes + qualifiedNameMsg);
				} else if (!notes.contains(qualifiedName)) { // do not search for qualifiedNameMsg, which is never found
																// probably due to the rendering of "\\n"
					
					// qualified name has been updated due to a package hierarchy change or renaming;
					// ==> update it accordingly by replacing the corresponding part of the notes
					int positionToReplace = notes.indexOf("\\n\\n");
					StringBuffer sbNotes = new StringBuffer(notes);
					if (positionToReplace > -1) {
						sbNotes.replace(positionToReplace, notes.length(), qualifiedNameMsg);
						EcoreUtil.setAnnotation(eClassifier, GENMODEL_ANNOTATION_SOURCE, DOCUMENTATION_ANNOTATION,
								sbNotes.toString());
					} else {
						logger.info("error on updating the fully qualified name annotation of " + qualifiedName);
					}
				} 
			}
		}
	}

	private String getOriginalQualifiedName(EPackage epkg, String name) {
		String pkgName = epkg.getName();
		String qualifiedName = pkgName + "." + name; //$NON-NLS-1$
		if (pkgName != model.getName()) {
			EPackage superPkg = epkg.getESuperPackage();
			return getOriginalQualifiedName(superPkg, qualifiedName);
		}

		// In the last recursion level...
		String modelName = model.getName() + ".";
		// ...find position behind leading "eastadl<version>. ..." 
		// (e.g., "eastadl22. ...)"
		int positionToInsertEadlPkgName = qualifiedName.indexOf(modelName) + modelName.length();
		// ...and insert "EAST-ADL." at this position, which is expected by EATOP
		// (e.g., "eastadl22.EAST-ADL. ...")
		StringBuffer sbQualifiedName = new StringBuffer(qualifiedName);
		sbQualifiedName.insert(positionToInsertEadlPkgName, EASTADL_PKG_NAME);

		return sbQualifiedName.toString();
	}

}