/**
 * <copyright>
 *  
 * Copyright (c) 2014 itemis and others.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 
 * which accompanies this distribution, and is
 * available at http://www.eclipse.org/org/documents/epl-v10.php
 *  
 * Contributors: 
 *     itemis - Initial API and implementation
 *  
 * </copyright>
 * 
 */
package org.eclipse.eatop.genmodelcodegen.postprocessings;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class FlattenPackages extends PostProcessingTemplate {

	private IFile ecoreIFile;

	public FlattenPackages(IFile ecoreIFile) {
		this.ecoreIFile = ecoreIFile;
	}

	@Override
	public void execute() throws Exception {
		EList<EPackage> subpackages = model.getESubpackages();
		for (EPackage pkg : subpackages) {
			flattenPackage(model, pkg);
		}
		model.getESubpackages().clear();

		// Adaptation Joerg Holtmann:
		// needed by the GenModel Postprocessings, which are executed subsequently and
		// fail with wrong package path references otherwise.
		// from org.eclipse.eatop.eaadapter.EAAdapter#saveEcore(...)
		Resource myResource = new ResourceSetImpl()
				.createResource(URI.createURI(this.ecoreIFile.getLocationURI().toString()));
		myResource.getContents().add(model);
		myResource.save(null);
	}

	private void flattenPackage(EPackage rootPkg, EPackage pkg) {
		final EList<EPackage> subpkgs = pkg.getESubpackages();
		for (EPackage subpkg : subpkgs) {
			flattenPackage(pkg, subpkg);
		}

		rootPkg.getEClassifiers().addAll(pkg.getEClassifiers());
	}
}
