package org.eclipse.eatop.genmodelcodegen.ui.handlers;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.eatop.genmodelcodegen.GenmodelCodeGenerator;
import org.eclipse.eatop.genmodelcodegen.ui.Activator;
import org.eclipse.eatop.genmodelcodegen.util.IConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.sphinx.platform.util.PlatformLogUtil;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

public class EASTADLGenmodelCodeGenUIHandler extends AbstractHandler {
	
	protected static String GENERATE_GENMODEL_AND_CODE = "org.eclipse.eatop.genmodelcodegen.ui.commandParameters.generateGenmodelAndCode"; 

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IFile selectedEcoreFile = unwrap(event, IFile.class);
		Shell shell = HandlerUtil.getActiveShell(event);

		if (selectedEcoreFile != null) {
			if (IConstants.ECORE_FILE_EXTENSION.equals(selectedEcoreFile.getFileExtension())) {
				String generateGenmodelAndCode = event.getParameter(GENERATE_GENMODEL_AND_CODE);

				// Create EASTADLEcoreMMGen
				final GenmodelCodeGenerator genmodelCodeGen = new GenmodelCodeGenerator(selectedEcoreFile, Boolean.parseBoolean(generateGenmodelAndCode)); 				
				// Progress monitor dialog
				ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);
				try {
					dialog.run(true, true, new IRunnableWithProgress() {
						@Override
						public void run(IProgressMonitor monitor) {
							doRun(genmodelCodeGen, monitor);
						}
					});
				} catch (InvocationTargetException e) {
					PlatformLogUtil.logAsError(Activator.getDefault(), e.getMessage());
				} catch (InterruptedException e) {
					PlatformLogUtil.logAsError(Activator.getDefault(), e.getMessage());
				}
			}
		} else {
			MessageDialog.openInformation(shell, "Info", "Please select an ecore file"); 
		}
		return null;
	}

	
	private <T> T unwrap(Object o, Class<T> clazz) {
		if (o instanceof ExecutionEvent) {
			o = HandlerUtil.getCurrentSelection((ExecutionEvent) o);
		}

		if (o instanceof IStructuredSelection) {
			o = ((IStructuredSelection) o).getFirstElement();
		}

		if (o instanceof IAdaptable) {
			o = ((IAdaptable) o).getAdapter(clazz);
		}

		if (clazz.isInstance(o)) {
			return clazz.cast(o);
		}

		return null;
	}
	

	private void doRun(GenmodelCodeGenerator genmodelCodeGen, IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, "Generating EATOP Genmodel file and code...", 100); //$NON-NLS-1$
		if (progress.isCanceled()) {
			throw new OperationCanceledException();
		}

		try {
			genmodelCodeGen.generate(monitor);
		} catch (Exception e) {
			PlatformLogUtil.logAsError(Activator.getDefault(), e);
		}
	}	
}
